(ns joshbatch.routes
  (:require (compojure [core :refer [defroutes GET PUT]])
            [compojure.handler :as handler]
            [ring.middleware.json :refer [wrap-json-body]]
            [ring.util.response :as response]
            [compojure.route :as route]))

(defroutes main-routes
  (GET "/" []
       "Hello World")
  (GET "/index" []
       (response/resource-response "index.html" {:root "public"}))
  (route/resources "/")
  (route/not-found "Page not found"))

(defroutes app-routes
  (wrap-json-body main-routes))
