(defproject joshbatch "0.1.0-SNAPSHOT"
  :description "Basic Website"
  :url "www.jbat.ch"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [compojure "1.1.6"]
                 [cheshire "5.3.1"]
                 [ring/ring "1.2.1"]
                 [ring/ring-json "0.3.1"]
                 [http-kit "2.1.16"]
                 [org.clojure/data.json "0.2.4"]]
  :plugins [[lein-ring "0.8.5"]]
  :ring {:handler joshbatch.routes/app-routes}
  :profiles {:dev {:dependencies [[midje "1.6.3"]
                                  [ring-mock "0.1.5"]]}})
